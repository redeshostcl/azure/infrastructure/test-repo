terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.46.1"
    }
  }
}

provider "azurerm" {
  # Configuration options
  features {}
}

#we get the IT resource group in which we want to create the nw watcher
data "azurerm_resource_group" "rg-it-dev-001" {
  name = "rg-it-dev-001"
}

resource "azurerm_network_watcher" "nww-dev-001" {
  name                = "nwwatcher-dev-westeu-001"
  location            = data.azurerm_resource_group.rg-it-dev-001.location
  resource_group_name = data.azurerm_resource_group.rg-it-dev-001.name
  tags = {
    Environment = "Development",
    OpsTeam     = "IT",
    Workload    = "Development Subscription"
  }
}